#include <iostream>
#include "Contenedor.h"

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
    private:
        int tope = 0;
        int max;
        Contenedor *arreglo_pila = NULL;
        bool band;

    public:
        Pila(int max);
        void Pila_vacia();
        void Pila_llena();
        void Push();
        void Pop(int dato);
        void Imprimir_arreglo_pila();
};
#endif
