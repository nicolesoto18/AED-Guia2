#include <iostream>
#include "Pila.h"
#include "Contenedor.h"

using namespace std;

void llenar_puerto(int tamano_puerto, int max, Pila **puerto){
    int dato;

    for(int i = 0; i < tamano_puerto; i++){
        puerto[i] = new Pila(max);
   
        for (int j = 0; j < max; j++){
            puerto[i]->Push();
        }
    }   
}


void imprimir_puerto(int tamano_puerto, Pila **puerto){
    // Se imprime la pila horizontalmente
    for(int i = 0; i < tamano_puerto; i++){
        puerto[i]->Imprimir_arreglo_pila();
    }
}


void menu(Pila **puerto, int max, int tamano_puerto){
    int opcion;
    int dato;
    
    cout << "\nMENU\n" << endl;
    cout << " [1] Agregar elementos\n [2] Eliminar elementos\n [3] Ver puerto" << endl;
    cout << " [4] Salir" << endl;
     
    cout << "\n Ingrese su opción: ";
    cin >> opcion;
    
    system("clear");

    while(getchar() != '\n');

    switch(opcion){
        case 1:
            llenar_puerto(tamano_puerto, max, puerto);
            menu(puerto, max, tamano_puerto);
            break;

        case 2:
            cout << "Elimnación no habilitado: " << endl;
            menu(puerto, max, tamano_puerto);
            break;

        case 3:
            imprimir_puerto(tamano_puerto, puerto);
            menu(puerto, max, tamano_puerto);
            break;

        case 4:
            break;

    }
}

    
int main(){
    int max = 0;
    int tamano_puerto;
    Pila *puerto[tamano_puerto];

    cout << "\n\tPUERTO SECO" << endl;

    cout << "Tamaño del puerto seco: ";
    cin >> tamano_puerto;

    cout << "Tamaño máximo de la pila: ";
    cin >> max;

    menu(puerto, max, tamano_puerto);

    return 0;
} 
