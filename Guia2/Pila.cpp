#include <iostream>
#include "Pila.h"
#include "Contenedor.h"

using namespace std;

Pila::Pila(int max){
    this->max = max;
    this->arreglo_pila = new Contenedor [this->max];
}


void Pila::Pila_vacia(){
    if(this->tope == 0){
        // La pila esta vacia
        this->band = true; 
    }
    
    else{
        // La pila esta llena
        this->band = false;
    }

}


void Pila::Pila_llena(){
    if(this->tope == this->max){
        // La pila esta vacia
        this->band = true;
    }
    
    else{
        // La pila no esta llena
        this->band = false;
    }
    
}


void Pila::Push(){
    Pila_llena();

    if (this->band == true){
        cout << "Desbordamiento, Pila llena" << endl;
    }
    
    else{
        this->tope = this->tope + 1;
        // Se añade el elemento en el nuevo tope
        this->arreglo_pila[this->tope - 1].add_datos();
    }
}


/*void Pila::Pop(int dato){
    Pila_vacia();

    if (this->band == true){
        cout << "Subdesbordamiento, Pila vacı́a";
    }
    
    else{
        dato = this->arreglo_pila[this->tope];
        this->tope = this->tope - 1;
    }
}*/


void Pila::Imprimir_arreglo_pila(){

    cout << '\t';

    for(int i = this->tope; i >= 1; i--){
        cout << "|" << arreglo_pila[i-1].get_nombre() << " -- " << arreglo_pila[i-1].get_empresa() << "|";
    }

    cout << "\n";
}

