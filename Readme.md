				    
                                                        	 PUERTO SECO        
							                            ---------------------
+ Empezando
    El programa cosiste en la creación de una puerto seco que contendra pilas de contenedores, inicialmente se pedira que ingrese el tamaño del puerto y el máximo de la pila, luego se presenta un menú con 4 opciones. La primera opción es agregar contenedores para lo cual se le pedira que ingrese un nombre de identificación y el nombre de la empresa. La segunda opción permite eliminar elementos (No habilitado). En la tercera opción se mostrara el contenido del puerto de forma horizontal.
        ~Ejemplo output:
            Pila 1 -> |1 -- Empresa1||2 -- Empresa2|
            Pila 2 -> |3 -- Empresa3||4 -- Empresa4|

    Finalmente la cuarta opción le permite al usuario cerrar el programa.


+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	    [2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	    [3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa
         

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.

